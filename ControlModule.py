import time
from Facial_Recognition_Module.FacialRecognition import FaceRecognition as fr
from Speech_Recognition_Module.SpeechRecognition import SpeechRecognition as sr
from ID_Scan_Module.IDScan import IDScan as ids
from Simulation_Module.ServeMeSimulator import ServeMeSimulatorManager as sim

import os
import re

def menu():
    """
    Funcion para mostrar el menu de configuracion de la maquina
    """
    global botAlc, botSin
    global listaNombres

    print("----------  CONFIGURACION  -------------")
    print("¿Cúantas botellas hay?")
    num = input("Inserta un número: ")
    print(f'Has seleccionado {num} botellas')
    print("")
    botAlc.clear()
    botSin.clear()
    listaNombres.clear()

    for botella in range(int(num)):
        print(f'¿La botella {botella+1} contiene alcohol?')
        opc = input("Inserta una opcion [s/n]: ")
        nom = input(f'Introduce el nombre de la bebida {botella+1}: ')
        print("")
        print("")

        if opc == 's':
            botAlc[botella] = [nom, opc]
            listaNombres.append(nom)
        else:
            botSin[botella] = [nom, opc]
            listaNombres.append(nom)

    print("RESULTADO CONFIGURACION: ", botAlc)
    print("Sin: ", botSin)
    print("--------------------")



def txtHistorial(user, bebida):
    file = open(f'./Historial/{user}.txt', 'a+')
    file.seek(0)
    ind = file.readline()

    if ind == '':
        file.seek(0)
        file.write("nPedidos: 0\n")
        file.close()
        file = open(f'./Historial/{user}.txt', 'a+')
        ind = "nPedidos: 0"
    
    

    ind = (re.findall('\d+', ind))

    ind = ind[0]

    print(ind)

    newInd = int(ind) + 1

    file.write("\n")
    file.write(f"======[{newInd}]======\n")
    file.write("\n")
    file.write(f"- {bebida}\n")
    file.write("\n")
    file.write("===============\n")

    aux = open(f'./Historial/{user}.txt', 'r')
    auxLines = aux.readlines()

    auxLines[0] = f"nPedidos: {newInd}\n"

    aux.close()

    aux = open(f'./Historial/{user}.txt', 'w')
    aux.writelines(auxLines)

    aux.close()

    #print(ind)
    
    file.close()



def getHist(user):
    if os.path.exists(f'./Historial/{user}.txt'):
        file = open(f'./Historial/{user}.txt', 'r')
        all_lines = file.readlines()
        nElem = len(all_lines)
        lastDrink = all_lines[nElem-3]
        return lastDrink
    else:
        return ""

    

#Al iniciar configuramos la maquina ("habra que guardarlo en un txt")
botAlc = {0: ['ron', 's'], 2: ['ginebra', 's']}
botSin = {1: ['fanta', 'n'], 3: ['coca-cola', 'n']}
listaNombres = ['ron', 'fanta', 'ginebra', 'coca-cola']
#menu()

#Instanciamos un objeto de la clase simulador
m = sim(listaNombres)
time.sleep(4)

#Inicializamos la edad a 18
edad = 18

while True:

    #Instanciamos un objeto de la clase FaceRecognition para el reconocimiento facial y IDScan para el escaneo del DNI
    facRec = fr()
    idScan = ids()

    #Esperamos hasta detectar una cara
    m.startSearchingFaces()
    frRet = facRec.reconocimientoFacial()
    m.stopSearchingFaces()
    
    #frRet = "juan_carlos"
    print("Res: ", frRet)

    #Instanciamos un objeto de la clase SpeechRecognition para la interacción con la voz
    spRec = sr()

    #Saludo por voz en función de si conoce o no a la persona detectada
    if frRet == "Desconocido": 
        spRec.respuestaVoz(frRet)

        m.startSearchingDNI()
        edad = idScan.reconocimientoEdadDNI()
        m.stopSearchingDNI()
        if edad < 18:
            spRec.respuestaVoz("menor")
        else:
            spRec.respuestaVoz("mayor")
    else: 
        lastDrink = getHist(frRet)
        spRec.respuestaVoz(f'conocido {frRet}')
        time.sleep(4)
        if(lastDrink != ""):
            spRec.respuestaVoz(f'historial {lastDrink}')
            time.sleep(4)
    #Esperamos para activar el micro
    time.sleep(3.5)

    if edad >= 18:
        #Leemos la respuesta del cliente
        m.startListening()
        srRet = spRec.reconocimientoVoz()
        m.stopListening()
        srRet = ' '.join(srRet).lower()
        #srRet = "fanta ron"
        print("res: ", srRet)

        #Si el comando de voz es "configuracion" y la persona es conocida mostramos el menu, sino ejecucion normal
        if (srRet == "configuracion" or srRet == "configuración") and frRet!="Desconocido":
            menu()
        else:
            auxAlcohol = False
            auxMezcla = False 
            auxPalSin = False
            auxPalMezcla = False

            for bot in botAlc.items():
                for word in srRet.split(' '):
                    if word in bot[1][0]:
                        auxAlcohol = True

            for bot in botSin.items():
                for word in srRet.split(' '):
                    if word in bot[1][0]:
                        auxMezcla = True

            if auxAlcohol:
                for word in srRet.split(' '):
                    if word == 'sin':
                        auxPalSin = True
                    if word == 'mezcla':
                        auxPalMezcla = True

            if (auxMezcla and auxAlcohol) or (auxPalSin and auxPalMezcla and auxAlcohol and not auxMezcla):
                spRec.respuestaVoz(srRet)
                time.sleep(4)
                #Guardem la posició actual del suport
                actualPos = 4
                #variables para comprobar si la bebida esta completa
                alc = False
                mez = False

                for bot in botAlc.items():
                    for word in srRet.split(' '):
                        if word in bot[1][0]:
                            if actualPos <= bot[0]:
                                m.moveRight()
                            else:
                                m.moveLeft()
                            m.waitDetection(bot[0])
                            m.stopMovement()
                            actualPos = bot[0]
                            alc = True
                            time.sleep(2)

                for bot in botSin.items():
                    for word in srRet.split(' '):
                        if word in bot[1][0]:
                            if actualPos <= bot[0]:
                                m.moveRight()
                            else:
                                m.moveLeft()
                            m.waitDetection(bot[0])
                            m.stopMovement()
                            actualPos = bot[0]
                            mez = True
                            time.sleep(2)

                m.moveRight()
                m.waitDetection(4)
                m.stopMovement()
                if (auxPalSin and auxPalMezcla and auxAlcohol and not auxMezcla):
                    spRec.respuestaVoz("sin mezcla")
                else:
                    spRec.respuestaVoz("finalizado")
                txtHistorial(frRet, srRet)
                time.sleep(3)
                
            else:
                spRec.respuestaVoz("no disponible")
                time.sleep(3)

m.stopSimulation()