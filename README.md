# ServeMe
<div style="text-align:center"><img src="./Modelado/Renders/ServeMe.gif" /></div>

# Table of Contents
  * [About this repository](#about-this-repository)
  * [What is ServeMe?](#what-is-serveme)
  * [Demo video](#demo-video)
  * [How to get up and running](#how-to-get-up-and-running)
  * [3D Renders and .stl files](#3d-renders-and-stl-files)
  * [Software modules and dependecies](#software-modules-and-dependencies)
    * [Facial Recognition Module](#facial-recognition-module)
      * Description
      * Requirements
    * [ID Card Scanning Module](#id-card-scanning-module)
      * Description
      * Requirements
    * [Speech Recognition/Generation Module](#speech-recognitiongeneration-module)
      * Description
      * Requirements
    * [Simulation Module](#simulation-module)
      * Description
      * Requirements
    * [Control Module](#control-module)
      * Description
      * Requirements

# About this repository
This repository contains all the developed software modules and 3D files that build up our robotics project "ServeMe".  
All the available code has been written by the components of the UAB Computer Engineering 2019-2020 RLP Group formed by Joel Moreno, Alex Blanes, Juan Carlos Soriano and Jorge Giménez.

# What is ServeMe?
ServeMe is a robot that will change the way you order drinks! ServeMe uses a simple but effective hardware-software architecture that enbales it to recognize you, listen to you, talk to you, and even scan someone's ID Card if it's not aware of who that person is! ServeMe will do everything for you as far as you want to interact with this super kind robot! :D 

ServeMe was first designed to be easily implemented using a RaspberryPi and the necessary hardware, but when the Covid-19 outbreak hit the world we were pushed to develop a full-software implementation of our idea. For that reason, you will find no hardware references (except the 3D models) in this repository. Instead, we created a 2D simulation that mimics the way the robot would behave.

# Demo video
[![Demo Video](READMEDemoVideo.png)](https://www.youtube.com/watch?v=EcjTVhDs0aY&feature=youtu.be)

# How to get up and running
1. Clone this repository
   > git clone https://gitlab.com/juankasv99/rlp
2. Make sure you have the latest version of [Python](https://www.python.org/) installed (it may work with lower versions) and that you're using Windows operative system (we're pretty close to be able to get it running on Linux and MacOS too).
3. Install the needed dependencies that you will find in the [Software Modules and dependencies](#software-modules-and-dependencies) section.
4. Go to [./ID_Scan_Module/IDScan.py line 16](./ID_Scan_Module/IDScan.py#L16) and change the path making it point to your Tesseract-OCR installation path.
5. Open your preferred **command line** (don't execute from an IDE or text editor, the simulator needs it's own process in the OS). We recommend Powershell.
6. Run `python ControlModule.py` from the [parent directory](.) and the program will guide you through the next steps! 

# 3D renders and .stl files
The .stl files and .png images of our 3D model for the robot can be found also in this repository [here](./Modelado).  
All the renders have been designed from scratch using Blender.  

<div style="text-align:center; height: 400px;"><img src="./Modelado/READMEComposition.png" /></div>

# Software modules and dependencies
<div style="text-align:center; height: 400px;"><img src="READMESofwareArquitecture.png" /></div>  

- ## Facial Recognition Module  
  - ### Description  
    The Facial Recognition Module is in charge of detecting and recognizing human faces.  
    It contains different callable functions:
      - The fist one enables the robot to train a model with 100 images of the same face. After that, ServeMe is able to remember and detect that face.
      - The second function is the one that can use the previous trained model to recognize the detected faces and return if those are known to teh robot or not along with a similarity percentage.    
  - ### Requirements
    - [OpenCV-Python](https://pypi.org/project/opencv-python/)
    - [NumPy](https://pypi.org/project/numpy/)
    - [PyTesseract](https://pypi.org/project/pytesseract/)
    - Some more Python standard libraries that don't need any installation process.
- ## ID Card Scanning Module
  - ### Description
    The ID Card Scanning Module is fired when the face seen by ServeMe is not known to it. This module uses OCR in order to extract the date of birth from the customer's ID Card and return if that person has the needed age to drink what the robot has to offer.  
    It uses (as can be seen in the dependencies) PyTesseract enchanced with our own image transformations to increase it's performance. To sum up, we converted the image to a black&white frame that then gets thresholded and the median-blured to reduce noise and make the content of the ID Card cleaner.  
    Finally regular expressions are used to easily find the desired data.
  - ### Requirements  
     This module has the same requirements as the previous one.
- ## Speech Recognition/Generation Module
  - ### Description  
    This module is composed by two main functions that enable ServeMe to listen and talk:
    - The first function uses the microphone and Google Speech Recognition API in order to perform a speech-to-text conversion of what the customer is asking for.
    - The second one manages the answers ServeMe does depending on a list of customized interactions. Then, it generates and executes an audio file with the answer. 
  - ### Requirements
    - [SpeechRecognition](https://pypi.org/project/SpeechRecognition/)
    - [gTTS](https://pypi.org/project/gTTS/)
    - Some more Python standard libraries that don't need any installation process.
- ## Simulation Module  
  - ### Description
    The Simulation Module consists of a 2D simulation of the robot we planned to implement in hardware. Using PyGame and Sockets we achived a beautiful simulation that mimics in a pretty accurate way how the real robot would behave.  
    We put special effort in making the simulation module easy to use by abstracting a lot of complexity and focusing on the module doing only what the real robot could do, no less, no more.
  - ### Requirements
    - [PyGame](https://pypi.org/project/pygame/)
    - Some more Python standard libraries that don't need any installation process.
- ## Control Module
  - ### Description  
    The Control Module is in charge of starting up the whole system and is the bridge between all of the input/output modules and the simulation one.  
    This module executes all the logic necessary to achieve the expected behaviour by taking care of the data that it recieves from calling methods from the rest of modules.
  - ### Requirements  
    - [SpeechRecognition](https://pypi.org/project/SpeechRecognition/)
    - Some more Python standard libraries that don't need any installation process.