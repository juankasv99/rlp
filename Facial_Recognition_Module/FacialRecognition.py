import cv2
import os
import numpy


class FaceRecognition:
    def __init__(self):
        self.creadores= {'joel_moreno': 0, 'alex_blanes': 0, 'jordi_gimenez': 0, 'juan_carlos': 0}
        self.desconocido = 0
        
        
        """
        ESTA FUCNCIÓN CALCULA LAS VECES QUE APARECE LA CARA DEL CREADOR O DE UNA PERSONA DESCONOCIDA
        """
    def reconocerCreador(self, cara):        
        if cara in self.creadores.keys():
            self.creadores[cara] += 1
        else:
            self.desconocido += 1
    
        """
        ESTA FUCNCIÓN BUSCA CARAS Y LAS RECONOCE
        """
    def reconocimientoFacial(self):
        # Parte 1: Creando el entrenamiento del modelo
        print('Bienvenido al programa de Reconocimiento Facial')
        
        #Directorio donde se encuentran las carpetas con las caras de entrenamiento
        dir_faces = './Facial_Recognition_Module/att_faces/orl_faces'
        
        #Tamaño para reducir a miniaturas las fotografias
        size = 4
        
        # Crear una lista de imagenes y una lista de nombres correspondientes
        (images, lables, names, id) = ([], [], {}, 0)
        for (subdirs, dirs, files) in os.walk(dir_faces):
            for subdir in dirs:
                names[id] = subdir
                subjectpath = os.path.join(dir_faces, subdir)
                print("subjpath: ", subjectpath)
                for filename in os.listdir(subjectpath):
                    path = subjectpath + '/' + filename
                    lable = id
                    images.append(cv2.imread(path, 0))
                    lables.append(int(lable))
                id += 1
        (im_width, im_height) = (112, 92)
        
        # Crear una matriz Numpy de las dos listas anteriores
        (images, lables) = [numpy.array(lis) for lis in [images, lables]]
        
        # OpenCV entrena un modelo a partir de las imagenes (SE PUEDE CAMBIAR EL MODELO COMENTANDO/DESCOMENTANDO LAS SIGUIENTES 3 LÍNEAS)
        model = cv2.face.LBPHFaceRecognizer_create()
        #model = cv2.face.EigenFaceRecognizer_create()
        #model = cv2.face.FisherFaceRecognizer_create()
        model.train(images, lables)
        
        
        # Parte 2: Utilizar el modelo entrenado en funcionamiento con la camara
        face_cascade = cv2.CascadeClassifier('./Facial_Recognition_Module/haarcascade_frontalface_default.xml')
        cap = cv2.VideoCapture(0)
        print('¡Modelo creado, empecemos!\n\n')
        
        while True:
            #leemos un frame y lo guardamos
            rval, frame = cap.read()
            frame=cv2.flip(frame,1,0)
        
            #convertimos la imagen a blanco y negro    
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        
            #redimensionar la imagen
            mini = cv2.resize(gray, (int(gray.shape[1] / size), int(gray.shape[0] / size)))
        
            #buscamos las coordenadas de los rostros (si los hay) y guardamos su posicion
            faces = face_cascade.detectMultiScale(mini)
            
            for i in range(len(faces)):
                face_i = faces[i]
                (x, y, w, h) = [v * size for v in face_i]
                face = gray[y:y + h, x:x + w]
                face_resize = cv2.resize(face, (im_width, im_height))
        
                # Intentado reconocer la cara
                prediction = model.predict(face_resize)
                
                 #Dibujamos un rectangulo en las coordenadas del rostro
                cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 3)
                
                # Escribiendo el nombre de la cara reconocida
                # La variable cara tendra el nombre de la persona reconocida
                cara = '%s' % (names[prediction[0]])
        
                #Si la prediccion tiene una exactitud menor a 100 se toma como prediccion valida
                if prediction[1]<100 :
                  #Ponemos el nombre de la persona que se reconoció
                  cv2.putText(frame,'%s - %.0f' % (cara,prediction[1]),(x-10, y-10), cv2.FONT_HERSHEY_PLAIN,1,(0, 255, 0))
        
                #Si la prediccion es mayor a 100 no es un reconomiento con la exactitud suficiente
                elif prediction[1]>101 and prediction[1]<500:           
                    #Si la cara es desconocida, poner desconocido
                    cv2.putText(frame, 'Desconocido',(x-10, y-10), cv2.FONT_HERSHEY_PLAIN,1,(0, 255, 0))  
                    cara = 'Desconocido'
                    
                    
                #En caso de que la cara sea de algun conocido se realizara determinadas acciones          
                #Busca si los nombres de las personas reconocidas estan dentro de los que tienen acceso   
                self.reconocerCreador(cara)
                  
                #Mostramos la imagen
                cv2.imshow('Pulsa ESC para salir del Reconocimiento Facial', frame)
                
                #Si una cara (conocida o desconocida) aparece mas de X veces, cerramos el programa y devolvemos la cara
                if(self.desconocido == 30):
                    cv2.destroyAllWindows()
                    return 'Desconocido'
                elif(self.creadores[max(self.creadores, key=self.creadores.get)] == 30):
                    cv2.destroyAllWindows()
                    return max(self.creadores, key=self.creadores.get)
             
                            
        
            #Si se presiona la tecla ESC se cierra el programa
            key = cv2.waitKey(10)
            if key == 27:
                cv2.destroyAllWindows()
                break
        
        
        
        """
        ESTA FUCNCIÓN GUARDA CARAS PARA LUEGO RECONOCERLAS
        """
    def capturarCara(self):
        nombre = input("Introduce tu nombre: ")
        print(f"Voy a tomar fotos de tu cara, {nombre}\n")
        
        #Directorio donde se encuentra la carpeta con el nombre de la persona
        dir_faces = 'att_faces/orl_faces'
        path = os.path.join(dir_faces, nombre)
        
        #Tamaño para reducir a miniaturas las fotografias
        size = 4
        
        #Si no hay una carpeta con el nombre ingresado entonces se crea
        if not os.path.isdir(path):
            os.mkdir(path)
        
        #cargamos la plantilla e inicializamos la webcam
        face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
        cap = cv2.VideoCapture(0)
        
        img_width, img_height = 112, 92
        
        #Ciclo para tomar fotografias
        count = 0
        parar = 0
        while parar != 2:
            #leemos un frame y lo guardamos
            rval, img = cap.read()
            img = cv2.flip(img, 1, 0)
        
            #convertimos la imagen a blanco y negro
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        
            #redimensionar la imagen
            mini = cv2.resize(gray, (int(gray.shape[1] / size), int(gray.shape[0] / size)))
        
            #buscamos las coordenadas de los rostros (si los hay) y guardamos su posicion
            faces = face_cascade.detectMultiScale(mini)    
            faces = sorted(faces, key=lambda x: x[3])
            
            if faces:
                face_i = faces[0]
                (x, y, w, h) = [v * size for v in face_i]
                face = gray[y:y + h, x:x + w]
                face_resize = cv2.resize(face, (img_width, img_height))
                
                if parar == 0:
                    #Dibujamos un rectangulo en las coordenadas del rostro
                    cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 3)
                    #Ponemos el nombre en el rectagulo
                    cv2.putText(img, nombre, (x - 10, y - 10), cv2.FONT_HERSHEY_PLAIN,1,(0, 255, 0))        
                else:
                    cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 3)
                    cv2.putText(img, "Listo, presiona 'esc'", (x - 10, y - 10), cv2.FONT_HERSHEY_PLAIN,1,(0, 0, 255))
                    
                if(parar == 0):
                    #El nombre de cada foto es el numero del ciclo
                    #Obtenemos el nombre de la foto
                    #Despues de la ultima sumamos 1 para continuar con los demas nombres
                    pin=sorted([int(n[:n.find('.')]) for n in os.listdir(path)
                           if n[0]!='.' ]+[0])[-1] + 1
            
                    #Metemos la foto en el directorio
                    cv2.imwrite('%s/%s.png' % (path, pin), face_resize)
            
                    #Contador del ciclo
                    count += 1
        
            #Mostramos la imagen
            cv2.imshow('Captura de la cara de '+nombre, img)
            
            if count >= 100 and parar == 0:
                print("Listo, recordaré tu cara. Presiona 'esc' para cerrar el programa\n")
                parar = 1
                
            #Si se presiona la tecla ESC se cierra el programa
            key = cv2.waitKey(10)
            if key == 27:
                cv2.destroyAllWindows()
                parar = 2
