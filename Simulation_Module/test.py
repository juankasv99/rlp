import ServeMeSimulator as sim
from time import sleep

# This script is used to test all the features of the simulator! :D #

m = sim.ServeMeSimulatorManager(['Drink 1', 'Drink 2', 'Drink 3', 'Drink 4'])

m.moveRight()
m.waitDetection(1)
m.stopMovement()
sleep(2)
m.moveLeft()
m.waitDetection(0)
m.stopMovement()
sleep(2)

m.moveRight()
m.waitDetection(2)
m.stopMovement()
sleep(2)
m.moveLeft()
m.waitDetection(0)
m.stopMovement()
sleep(2)

m.moveRight()
m.waitDetection(3)
m.stopMovement()
sleep(2)
m.moveLeft()
m.waitDetection(0)
m.stopMovement()
sleep(2)

m.moveRight()
m.waitDetection(4)
m.stopMovement()
sleep(2)
m.moveLeft()
m.waitDetection(0)
m.stopMovement()
sleep(2)

sleep(3)
m.startListening()
sleep(3)
m.stopListening()
sleep(3)
m.startSearchingFaces()
sleep(3)
m.stopSearchingFaces()
sleep(3)

m.stopSimulation()