import socket
import pygame
import subprocess
import time
import sys


###   ---START ENVIRONMENT RELATED DEFINITIONS---   ###
# THIS IS A 'HELPER' CLASS DON'T INSTANTIATE IT TO RUN THE SIMULATION, USE THE ONE A THE BOTTOM INSTEAD! :D #  
class ServeMeSimulatorEnvironment:
    def simulatorPrint(self, str):
        print(f"===> ServeMeSimulatorEnvironment: {str}")

    def __init__(self, names = [], screenWidth = 1080, screenHeight = 680, socketPort = 8000, movementSpeed = 3, backgroundImagePath = "./Simulation_Module/background.jpg"):
        self.simulatorPrint("ServeMe simulator has been created.")

        pygame.init()
        self.names = Names(names)
        self.screenWidth = screenWidth #Ancho de la pantalla
        self.screenHeight = screenHeight #Alto de la pantalla
        self.backgroundImagePath = backgroundImagePath
        self.screenWindow = None
        self.font = pygame.font.SysFont(pygame.font.get_default_font(), 50)
        self.listeningText = self.font.render("Listening...", True, (0,0,0))
        self.searchingFacesText =  self.font.render("Searching for faces...", True, (0,0,0))
        self.searchingDNIText =  self.font.render("Searching for the date of birth on the DNI...", True, (0,0,0))

        self.socketPort = socketPort #Puerto donde estará escuchando a conexión
        self.socket = socket.socket() #Objeto socket
        self.socketConnection = None #Inicializamos la conexión en nulo

        self.serveMeMachine = None
        self.detectors = None
        self.movementSpeed = movementSpeed

        self.currentOrder = None #Comando que está ejecutando ahora mismo el robot
        self.isSimulationRunning = True #Inicializamos la variables de control del loop de la simulación
        self.isListening = False #Inicializamos la variable de control para la función de escucha
        self.isSearchingFaces = False #Inicializamos la variable de control para la función de detección facial
        self.isSearchingDNI = False #Inicializamos la variable de control para la función de detección de edad en el DNI
        self.initiateSimulation() #Iniciamos el loop de simulación

    #def drawTestingSquare(self):


    def initiateSimulation(self):
        self.socket.bind(('localhost', self.socketPort)) #Escuchamos en el puerto indicado
        self.socket.listen(1) #Sólo aceptamos una conexión
        self.socketConnection, _ = self.socket.accept() #Esperamos la conexión y cuando esté disponible nos guardamos la conexión con el cliente (MANAGER)
        self.socketConnection.setblocking(0)

        clock = pygame.time.Clock()

        pygame.display.set_caption("ServeMe Simulator") #Título para la ventana
        backgroundImage = pygame.image.load(self.backgroundImagePath)
        self.screenWindow = pygame.display.set_mode((self.screenWidth, self.screenHeight)) #Iniciamos la vetana de simulación
        self.serveMeMachine = ServeMeMachine(self.screenWindow)
        self.detectors = Detectors([Detector(self.screenWindow, 177, 404), Detector(self.screenWindow, 352, 404), Detector(self.screenWindow, 533, 404), Detector(self.screenWindow, 698, 404), Detector(self.screenWindow, 940, 404)])

        while(self.isSimulationRunning):
            clock.tick(60)
            for event in pygame.event.get():
                pass

            self.detectors.checkStatus(self.serveMeMachine.detectionPosition)
            if True in self.detectors.detectorsStatus:
                pos = self.detectors.detectorsStatus.index(True)
                self.socketConnection.send(str(pos).encode('utf8'))

            self.screenWindow.fill([102, 153, 255]) #Pintar el fondo
            self.screenWindow.blit(backgroundImage, (0,0))
            self.handleSocketEvents() #Tratar los eventos que se reciven por el socket
            self.serveMeMachine.draw()
            #self.detectors.draw()
            self.names.draw(self.screenWindow)
            if self.isSearchingFaces: self.screenWindow.blit(self.searchingFacesText, (self.screenWidth/2 - self.searchingFacesText.get_width()/2, self.screenHeight/2 - self.searchingFacesText.get_height()/2))
            if self.isListening: self.screenWindow.blit(self.listeningText, (self.screenWidth/2 - self.listeningText.get_width()/2 ,self.screenHeight/2 - self.listeningText.get_height()/2))
            if self.isSearchingDNI: self.screenWindow.blit(self.searchingDNIText, (self.screenWidth/2 - self.searchingDNIText.get_width()/2 ,self.screenHeight/2 - self.searchingDNIText.get_height()/2))

            pygame.display.update() #Repintar la pantalla
    
    def handleSocketEvents(self):
        try:
            receivedData = self.socketConnection.recv(1024)
            if receivedData != b'':
                receivedData = int(receivedData.decode('utf-8'))
                self.currentOrder = receivedData
            self.executeOrder()
        except:
            self.executeOrder()
            return
    
    def executeOrder(self):
        if self.currentOrder == 1:
            #self.simulatorPrint("Move right")
            self.serveMeMachine.x += self.movementSpeed
            self.serveMeMachine.detectionPosition[0] += self.movementSpeed
        elif self.currentOrder == 2:
            #self.simulatorPrint("Move left")
            self.serveMeMachine.x -= self.movementSpeed
            self.serveMeMachine.detectionPosition[0] -= self.movementSpeed
        elif self.currentOrder == 3:
            self.simulatorPrint("Stop!")
        elif self.currentOrder == 4:
            self.isSimulationRunning = False
        elif self.currentOrder == 5:
            self.isListening = True
        elif self.currentOrder == 6:
            self.isListening = False
        elif self.currentOrder == 7:
            self.isSearchingFaces = True
        elif self.currentOrder == 8:
            self.isSearchingFaces = False
        elif self.currentOrder == 9:
            self.isSearchingDNI = True
        elif self.currentOrder == 10:
            self.isSearchingDNI = False

class ServeMeMachine:
    def __init__(self, screenWindow, x = 685, y = 0):
        self.screenWindow = screenWindow
        self.x = x
        self.y = y
        self.machineSpritePath = "./Simulation_Module/sprites/renderFinalNoDispensers.png"
        self.dispensersSpritePath = "./Simulation_Module/sprites/renderFinalOnlyDispensers.png"
        self.supportSpritePath = "./Simulation_Module/sprites/renderFinalSoloBandeja.png"
        self.detectionPosition = [685+248, 409]
    
    def draw(self):
        machineSprite = pygame.image.load(self.machineSpritePath)
        supportSprite = pygame.image.load(self.supportSpritePath)
        dispensersSprite = pygame.image.load(self.dispensersSpritePath)

        self.screenWindow.blit(machineSprite, (0,0))
        self.screenWindow.blit(supportSprite, (self.x,self.y))
        self.screenWindow.blit(dispensersSprite, (0,0))

        #pygame.draw.circle(self.screenWindow, pygame.Color(255,0,0,50), self.detectionPosition, 3)

class Detector:
    def __init__(self, screenWindow, x, y):
        self.x = x
        self.y = y
        self.screenWindow = screenWindow
        self.lineLength = 100

    def draw(self):
        pygame.draw.line(self.screenWindow, pygame.Color(255,0,0,50), (self.x, self.y), (self.x, self.y+self.lineLength), 3)

class Detectors:
    def __init__(self, detectors):
        self.nDetectors = len(detectors)
        self.detectors = detectors
        self.detectorsStatus = [False]*self.nDetectors
    
    def draw(self):
        for d in self.detectors:
            d.draw()

    def checkStatus(self, glassPosition):
        for i in range(0, self.nDetectors):
            if glassPosition[0] == self.detectors[i].x or glassPosition[0] == self.detectors[i].x+1 or glassPosition[0] == self.detectors[i].x-1:
                self.detectorsStatus[i] = True
            else: self.detectorsStatus[i] = False

class Names:
    def __init__(self, names):
        self.nNames = len(names)
        self.names = names
        self.font = pygame.font.SysFont(pygame.font.get_default_font(), 20)
        self.renderedTexts = []
        for name in self.names:
            self.renderedTexts.append(self.font.render(name, True, (255,255,255)))
    
    def draw(self, screenWindow):
        for i in range(self.nNames):
            screenWindow.blit(self.renderedTexts[i], (160+170*i,200))
            #self.screenWindow.blit(self.searchingFacesText, (self.screenWidth/2 - self.searchingFacesText.get_width()/2, self.screenHeight/2 - self.searchingFacesText.get_height()/2))
        


###   ---FINISH ENVIRONMENT RELATED DEFINITIONS---   ###    




###   ---START MANAGER RELATED DEFINITIONS---   ###
# THIS IS THE CLASS YOU MUST INSTANTIATE IN ORDER TO SIMULATE! :D #            
class ServeMeSimulatorManager:
    def __init__(self, names, socketPort=8000):
        with open("./RunSimulationAuxScript.py", "w+") as auxFile:
            auxFile.write(
f"""
from Simulation_Module.ServeMeSimulator import ServeMeSimulatorEnvironment as sim
sim(names={str(names)})
"""
            )
            auxFile.close()
        
        subprocess.Popen([sys.executable, './RunSimulationAuxScript.py'], creationflags = subprocess.CREATE_NEW_CONSOLE)
        time.sleep(1)
        self.socketPort = socketPort
        self.socket = socket.socket()
        self.socket.connect(('localhost', self.socketPort)) #Nos conectamos al simulador (ENVIRONMENT)
    
    def moveRight(self):
        self.socket.send('1'.encode('utf8'))

    def moveLeft(self):
        self.socket.send('2'.encode('utf8'))
    
    def stopMovement(self):
        self.socket.send('3'.encode('utf8'))

    def stopSimulation(self):
        self.socket.send('4'.encode('utf8'))

    def startListening(self):
        self.socket.send('5'.encode('utf8'))
    
    def stopListening(self):
        self.socket.send('6'.encode('utf8'))

    def startSearchingFaces(self):
        self.socket.send('7'.encode('utf8'))

    def stopSearchingFaces(self):
        self.socket.send('8'.encode('utf8'))
    
    def startSearchingDNI(self):
        self.socket.send('9'.encode('utf8'))

    def stopSearchingDNI(self):
        self.socket.send('10'.encode('utf8'))
    

    def waitDetection(self, index):
        recvIdx = None
        while recvIdx != index:
            recvIdx = int(self.socket.recv(1024).decode('utf-8'))


###   ---FINISH MANAGER RELATED DEFINITIONS---   ###
    