from gtts import gTTS
import os
import speech_recognition as sr

class SpeechRecognition: 
    def __init__(self):
        pass
        
    def reconocimientoVoz(self):
        detectado = False
        r = sr.Recognizer()
        with sr.Microphone() as source:
        #with sr.AudioFile("audio.wav") as source:
            while(not detectado):
                print("Habla!!")
                r.adjust_for_ambient_noise(source)
                audio = r.listen(source)
                #audio = r.record(source) 
                print("Espera un momento...")
                try:
                    audio2text = r.recognize_google(audio, language="es-ES")
                    print("Has dicho: ", audio2text)
                    detectado = True
                except sr.UnknownValueError:
                    print("No te he entendido!")
                    #tts = gTTS("No te he entendido!", lang="es-es")
                    #tts.save("audio.mp3")
                    #os.system("audio.mp3")
                except sr.RequestError as e:
                    print("No he podido obtener resultados; {0}".format(e))
        
        textArray = audio2text.split(" ")
        
        return textArray
               
    def respuestaVoz(self, textArray): 
        auxAcabar = False
        textArray = textArray.replace('_' ,' ')
        textArray = textArray.lower()
        if "historial" in textArray:
            texto = f"La última vez me dijiste {textArray[10:]}"
            auxAcabar = True
        elif "tonto" in textArray or "tonta" in textArray:
            texto = "¡No pienso servir a gente tan maleducada!"
            auxAcabar = True
        elif "vodka" in textArray:
            texto = "Marchando un vodka. Vladimír Putin está orgulloso de ti"
        elif "ron" in textArray:
            texto = "Marchando un ron. Como un buen pirata"
        elif "tequila" in textArray:
            texto = "Marchando un tequila. Viva Méjico cabrones"
        elif "ginebra" in textArray or "ginevra" in textArray:
            texto = "Marchando una ginebra. Lástima que no tengamos tónica"
        elif "whiskey" in textArray or "whisky" in textArray:
            texto = "Marchando una copa de whiskey. Disfruta"
        elif "qué" in textArray and "tal" in textArray:
            texto = "Muy bien, gracias"
            auxAcabar = True
        elif "cómo" in textArray and "estás" in textArray:
            texto = "Muy bien, gracias"
            auxAcabar = True
        elif "desconocido" in textArray:
            texto = "Buenas, ¿serías tan amable de mostrarme el DNI para comprobar tu edad?"
        elif "mayor" in textArray:
            texto = "Estupendo, ¿qué deseas?"
        elif "menor" in textArray:
            texto = "Lo siento pero mis creadores no me permiten servir a menores de edad."
        elif "conocido" in textArray:
            texto = f'Buenas {textArray[9:]}, ¿qué deseas tomar hoy?'
        elif "finalizado" in textArray:
            texto = "La preparación ha terminado. Puedes recoger tu bebida."
        elif "no" in textArray and "disponible" in textArray:
            texto = "Lo siento, la bebida que has pedido no está disponible."
        elif "sin" in textArray and "mezcla" in textArray and auxAcabar==False:
            texto = "Sin mezcla te la juegas, pero ahí lo llevas."
        else:
            texto = "Lo siento pero no entiendo lo que dices"#"Texto de prueba"

        print(texto)
        tts = gTTS(texto, lang="es-es")
        tts.save("audio.mp3")
        os.system("audio.mp3")

        if "por" in textArray and "favor" in textArray and auxAcabar==False:
            texto = "A la gente educada le sirvo con gusto."
            tts = gTTS(texto, lang="es-es")
            tts.save("audio.mp3")
            os.system("audio.mp3")