import IDScan as IDS

aux = IDS.IDScan()
edad = aux.reconocimientoEdadDNI()

if edad <= 0:
    print("Error: Prueba a enseñar el DNI otra vez")
elif edad < 18:
    print("Eres menor, no deberías tomar bebidas alcohólicas")
else:
    print(f"Tienes {edad} años")
