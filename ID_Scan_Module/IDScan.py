import re
import pytesseract
import datetime
import cv2

class IDScan:
    def __init__(self):
        pass


        """
        ESTA FUCNCIÓN DETECTA LA FECHA EN EL DNI Y LA DEVUELVE
        """
    def reconocimientoEdadDNI(self):
        print("Enseñame la fecha de nacimiento del DNI \n")
        pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract'
        #C:\Users\joelm\AppData\Local\Programs\Python\Python38-32\Scripts
        age = 0
        cap = cv2.VideoCapture(0)
        parar = False
        while True:
            rval, img = cap.read()
            #img = cv2.flip(img, 1, 0)
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            thresh = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 31, 2)
            #thresh = cv2.threshold(gray, 150, 255, cv2.THRESH_BINARY)[1]
            #thresh = cv2.GaussianBlur(thresh, (3,3), 0)
            thresh = cv2.medianBlur(thresh, 3)
            thresh = cv2.medianBlur(thresh, 3)
            #img = cv2.bilateralFilter(thresh,9,75,75)
            data = pytesseract.image_to_string(thresh, lang='spa', config='--psm 6')
            #print(data)
            
            date_pattern = '(0[1-9]|[12][0-9]|3[01])\s(0[1-9]|1[012])\s(19|20)\d\d'
            date_string = re.search(date_pattern,data)
            if date_string: 
                aux = date_string.group(0)
                aux = aux.split(" ")
                fecha_nacimiento = datetime.date(int(aux[2]), int(aux[1]), int(aux[0]))
                today = datetime.date.today()
                age = today.year - fecha_nacimiento.year - ((today.month, today.day) < (fecha_nacimiento.month, fecha_nacimiento.day))
                parar = True
                
            cv2.imshow('Muestra la fecha de nacimiento del DNI', img)

            if(parar == True):
                cv2.destroyAllWindows()
                return age

            key = cv2.waitKey(10)
            if key == 27:
                cv2.destroyAllWindows()
                parar = True
        
        